package controller;

import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import model.*;

/**
 * 
 * @author John Strauser
 * <p>Controls the functionality of the slideshow feature in albumview.
 * Mainly allows user to have a simplified photo view of each photo in an album</p>
 *
 */
public class SlideshowController {
	@FXML AnchorPane anchorPane;
	public Album album;
	public int index;
	public int size;
	ImageView iv;
	
	/**
	 * 
	 * @param a - current album
	 * <p>Initializes the photo Imageview and the album</p>
	 */
	public void start(Album a){
		album = a;
		index = 0;
		size = album.getPhotos().size();
		
		//Set imageview to first photo in album
		iv = new ImageView(album.getPhoto(0).getPath());
		iv.setFitHeight(250.0);
		iv.setFitWidth(250.0);
		
		iv.setX(0);
		iv.setY(25);
		
		anchorPane.getChildren().add(iv);
	}
	/**
	 * @author John Strauser
	 * <p>Changes scene to the albumview of the current album</p>
	 */
	public void albumHandler(){
		Photos.changeScene("/view/AlbumView.fxml",album);
	}
	/**
	 * @author John Strauser
	 * <p>Moves the photo in slideshow to the next photo in the album</p>
	 */
	public void nextHandler(){
		if(index >= size-1){
			return;
		}
		anchorPane.getChildren().remove(anchorPane.getChildren().indexOf(iv));
		index++;
		iv = new ImageView(album.getPhoto(index).getPath());
		iv.setFitHeight(250.0);
		iv.setFitWidth(250.0);
		iv.setX(0);
		iv.setY(25);
		anchorPane.getChildren().add(iv);
	}
	/**
	 * @author John Strauser
	 * <p>Moves current photo in slideshow to the previous photo in the album.</p>
	 */
	public void prevHandler(){
		if(index <= 0){
			return;
		}
		anchorPane.getChildren().remove(anchorPane.getChildren().indexOf(iv));
		index--;
		iv = new ImageView(album.getPhoto(index).getPath());
		iv.setFitHeight(250.0);
		iv.setFitWidth(250.0);
		iv.setX(0);
		iv.setY(25);
		anchorPane.getChildren().add(iv);
	}
}
