package controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Optional;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.util.Callback;
import model.*;
/**
 * 
 * @author John Strauser
 * <p>Controls functionality that occurs within view of a specific photo</p>
 * <p>Allows user to copy the photo to another album, move the photo, delete the photo, edit its details, add, and remove tags on the photo</p>
 *
 */
public class PhotoViewController {
	@FXML Text Name;
	@FXML Text Date;
	@FXML Text Tags;
	@FXML HBox Hbox;
	
	public User user;
	public Album album;
	public Photo photo;
	
	/**
	 * @author John Strauser
	 * @param u - current user
	 * @param a - current album
	 * @param p - current photo selected
	 * <p>Initializes the user, album, and current photo (as well as the photos tags, date, and name)</p>
	 */
	public void start(User u, Album a, Photo p) {
		user=u;
		album = a;
		photo = p;
		
		//Load Photo and details and display in proper place
		updateNameText();
		Name.maxWidth(180.0);
		
		updateDateText();
		Date.maxWidth(180.0);
		
		updateTagsText();
		Tags.maxWidth(180.0);
		Tags.setWrappingWidth(180.0);
		
		ImageView iv = createImageView(p.getPath());
		Hbox.getChildren().add(iv);
	}
	/**
	 * 
	 * @param path
	 * @return - an Imageview object
	 * <p>Takes a path and returns an Imageview Object</p>
	 */
	public ImageView createImageView(String path){
		Image image = new Image(path);
		ImageView iv = new ImageView(image);
		
		iv.setFitWidth(Hbox.getMaxWidth());
		iv.setFitHeight(Hbox.getMaxHeight());
		
		return iv;
	}
	/**
	 * Changes scene back to the album that this current photo is in
	 */
	public void albumHandler() {
		Photos.changeScene("/view/AlbumView.fxml", album);
	}
	/**
	 * Logs the user out of the application
	 */
	public void logoutHandler() {
		//Do stuff before logout (maybe save)
		Photos.changeScene("/view/Login.fxml", 0);
	}
	/**
	 * @author John Strauser
	 * <p>Copies the current photo in photo view to another album</p>
	 * <p>Album is the one selected by the user in a dialog box</p>
	 */
	public void copyPhotoHandler() {
		ChoiceDialog<Album> dialog = new ChoiceDialog<Album>(user.getAlbum(0), user.getAlbums());
		dialog.setTitle("Copy Photo");
		dialog.setHeaderText("Select the album to copy the photo to.");
		 
		Optional<Album> result = dialog.showAndWait();
		if(!result.isPresent()){
			return;
		}
		Album toCopy = result.get();
/*		if (toCopy.equals(album)) {
			ControllerTools.showInputError(4);
			return;
		}*/
		toCopy.addPhoto(photo);
		if(toCopy.getPhoto(0).getName().equals("DefaultImgNameOnlyWeWouldPut")){
			toCopy.getPhotos().remove(0);
		}
	}
	/**
	 * @author John Strauser
	 * <p>User selects an album from a dialog box, the current photo is deleted from the current album it is in and moved to the selected album</p>
	 */
	public void movePhotoHandler() {
		ChoiceDialog<Album> dialog = new ChoiceDialog<Album>(user.getAlbum(0), user.getAlbums());
		dialog.setTitle("Move Photo");
		dialog.setHeaderText("Select the album to move the photo to.");
		 
		Optional<Album> result = dialog.showAndWait();
		if(!result.isPresent()){
			return;
		}
		Album toMove = result.get();
		
		//remove photo from current album
		album.getPhotos().remove(photo);
		if(album.getPhotos().size() == 0){
			album.addDefault();
		}
		
		//add photo to selected album
		toMove.addPhoto(photo);
		if(toMove.getPhoto(0).getName().equals("DefaultImgNameOnlyWeWouldPut")){
			toMove.getPhotos().remove(0);
		}
		album = toMove;
	}
	/**
	 * @author John Strauser
	 * <p>Edits the details of the current photo (caption and date)</p>
	 * <p>If an invalid date modification is made (not following format in the textfield of the dialog box) the edit is disallowed</p>
	 */
	public void editDetailsHandler() {
		//Popup dialog that returns photo
		Dialog<Photo> dialog = new Dialog<Photo>();
		dialog.setTitle("Edit Details");
		dialog.setHeaderText("Change the Caption and Date of the photo.");
		 
		Label captionLabel = new Label("Caption: ");
		Label dateLabel = new Label("Date: ");
		TextField captionText = new TextField();
		TextField dateText = new TextField();
		         
		captionText.setText(photo.getName());
		dateText.setText(""+photo.getDate().getTime());
		
		GridPane grid = new GridPane();
		grid.add(captionLabel, 1, 1);
		grid.add(captionText, 2, 1);
		grid.add(dateLabel, 1, 2);
		grid.add(dateText, 2, 2);
		dialog.getDialogPane().setContent(grid);
		         
		ButtonType buttonTypeOk = new ButtonType("Add", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
		 
		dialog.setResultConverter(new Callback<ButtonType, Photo>() {
		    @Override
		    public Photo call(ButtonType b) {
		 
		        if (b == buttonTypeOk) {
		        	Photo temp = photo;
		        	if(captionText.getText().equals("") || dateText.getText().equals("")){
		        		ControllerTools.showInputError(3);
		        		return null;
		        	}else{
		        		temp.setName(captionText.getText());
		        		SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
		        		try {
							temp.getDate().setTime(sdf.parse(dateText.getText()));
						} catch (ParseException e) {
							ControllerTools.showInputError(7); 
							
							return null;
						}
		        	}
		            return temp;
		        }
		 
		        return null;
		    }
		});
		         
		Optional<Photo> result = dialog.showAndWait();
		         
		if (result.isPresent() && result.get() != null) {
			Photo p =result.get();
			int index = album.getPhotos().indexOf(photo);
			album.getPhotos().set(index, p);
			photo = p;
		}
		updateNameText();
		updateDateText();
	}
	/**
	 * @author John Strauser
	 * <p>Adds a tag to the photo in photo view</p>
	 */
	public void addTagHandler() {
		Dialog<Tag> dialog = new Dialog<Tag>();
		dialog.setTitle("Add Tag");
		dialog.setHeaderText("Enter a tag name and value. Then click 'Add'.");
		 
		Label nameLabel = new Label("Name: ");
		Label valueLabel = new Label("Value: ");
		TextField nameText = new TextField();
		TextField valueText = new TextField();
		         
		GridPane grid = new GridPane();
		grid.add(nameLabel, 1, 1);
		grid.add(nameText, 2, 1);
		grid.add(valueLabel, 1, 2);
		grid.add(valueText, 2, 2);
		dialog.getDialogPane().setContent(grid);
		         
		ButtonType buttonTypeOk = new ButtonType("Add", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
		 
		dialog.setResultConverter(new Callback<ButtonType, Tag>() {
		    @Override
		    public Tag call(ButtonType b) {
		 
		        if (b == buttonTypeOk) {
		 
		            return new Tag(nameText.getText(), valueText.getText());
		        }
		 
		        return null;
		    }
		});
		         
		Optional<Tag> result = dialog.showAndWait();
		         
		if (result.isPresent()) {
			Tag t = result.get();
			if(t.getName().equals("") || t.getValue().equals("")){
				ControllerTools.showInputError(3);
				return;
			}
			photo.addTag(t);
		}
		updateTagsText();
	}
	/**
	 * @author John Strauser
	 * <p>Raises a dialog box with a drop down menu of all tags on the photo</p>
	 * <p>When user clicks OK, the selected tag in the dropdown menu is deleted.</p>
	 */
	public void deleteTagHandler() {
		if (photo.tags.size() == 0) {
			ControllerTools.showInputError(4);
			return;
		}
		ChoiceDialog<Tag> dialog = new ChoiceDialog<Tag>(photo.tags.get(0), photo.tags);
		dialog.setTitle("Delete Tag");
		dialog.setHeaderText("Select the tag to delete");
		 
		Optional<Tag> result = dialog.showAndWait();
		Tag toDelete; 
		         
		toDelete = result.get();
		 
		photo.tags.remove(toDelete);
		updateTagsText();
		
	}
	/**
	 * @author Joshua Pineda John Strauser
	 * <p>Deletes the current photo in photo view from the album
	 * First prompts the user confirmation before deleting the photo</p>
	 */
	public void deletePhotoHandler() {
		
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Dialog");
		alert.setHeaderText("Confirmation");
		alert.setContentText("Are you ok with deleting this photo?");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			album.photos.remove(photo);
			if(album.getPhotos().size()==0){
				album.addDefault();
			}
			Photos.changeScene("/view/AlbumView.fxml", album);
		}
		
		
	}
	/**
	 * @author John Strauser
	 * <p>Initializes the text of Tags in a photoview</p>
	 */
	public void updateTagsText(){
		String tagsText = "Tags: ";
		for(int i=0; i<photo.tags.size(); i++){
			Tag temp = photo.getTag(i);
			if(i==0){
				tagsText += temp.toString();
			}else{
				tagsText += " | "+temp.toString();
			}
			
		}
		Tags.setText(tagsText);
	}
	/**
	 * @author John Strauser
	 * <p>Initializes caption of photo in photoview</p>
	 */
	public void updateNameText(){
		Name.setText("Name: "+photo.getName());
	}
	/**
	 * @author John Strauser
	 * <p>Initializes the date of a photo in photoview</p>
	 */
	public void updateDateText(){
		Date.setText("Date: "+photo.getDate().getTime());
	}
}
