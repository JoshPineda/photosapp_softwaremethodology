package controller;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import model.Photos;

public class ControllerTools {
	/**
	 * @author Joshua Pineda
	 * @param errorType - the type of error
	 * @param username - Used for certain error messages to display error in more depth
	 * <p>Function mainly used to raise error dialog boxes when there are errors related to Users</p>
	 */
	public static void showUserError(int errorType, String username) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText("There was an error!");
		switch(errorType) {
			
			case 1:
				alert.setContentText("Error, username " + username + " cannot be found");
				alert.showAndWait();
				break;
			case 2:
				alert.setContentText("Error, username is null");
				alert.showAndWait();
				break;
			case 3:
				alert.setContentText("Error, username " + username + " already exists.");
				alert.showAndWait();
				break;
			case 4:
				alert.setContentText("Error, cannot copy photo to the same album");
				alert.showAndWait();
				break;

			
		}
	}
	/**
	 * @author Joshua Pineda
	 * @param errorType - type of error
	 * <p>Function used to raise error dialog boxes for input errors made by the user</p>
	 */
	public static void showInputError(int errorType) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText("There was an error!");
		switch(errorType) {
			case 1:
				alert.setContentText("Please enter an album name");
				alert.showAndWait();
				break;
			case 2:
				alert.setContentText("Please select an album to delete");
				alert.showAndWait();
				break;
			case 3:
				alert.setContentText("Input fields are blank");
				alert.showAndWait();
				break;
			case 4:
				alert.setContentText("There are no tags to delete");
				alert.showAndWait();
				break;
			case 5:
				alert.setContentText("No duplicate album names are allowed!");
				alert.showAndWait();
				break;
			case 6:
				alert.setContentText("Cannot create album from empty search results");
				alert.showAndWait();
				break;
			case 7:
				alert.setContentText("Invalid date entry");
				alert.showAndWait();
				break;
			case 8:
				alert.setContentText("Duplicate of same photo	 in album not allowed");
				alert.showAndWait();
				break;
		}
	}
	


}
