package controller;
import model.Album;
import model.Photo;
import model.Photos;
import model.User;
import javafx.fxml.FXML;
import javafx.geometry.Insets;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.TilePane;

/**
 * 
 * @author Joshua Pineda John Strauser
 * <p>Page where the application starts</p>
 * <p>Users can create albums, delete albums, view an album, or search for a photo</p>
 *
 */
public class HomePageController {
	@FXML TilePane tilePane;
	@FXML ScrollPane scrollPane;
	@FXML TextField SearchBox;
	
	
	public User user;
	/**
	 * @author Joshua Pineda John Strauser
	 * <p>Creates a dialog box that prompts user for an album name for their new album</p>
	 * <p>Upon completion, a blank album is created with the inputted name</p>
	 */
	private void CreateAlbumDialogBox() {
		TextInputDialog dialog = new TextInputDialog("Album Name");
		dialog.setTitle("Create Album");
		dialog.setHeaderText("Album Creation");
		dialog.setContentText("Please enter name for your album");

		// Traditional way to get the response value.
		Optional<String> albumname = dialog.showAndWait();
		if (albumname.isPresent()){
			if (albumname.get().equals(null)) {
				ControllerTools.showInputError(1);
				return;
			}
			if(!checkAlbumName(albumname.get())) {
				Album tempalbum = new Album(albumname.get());
			    user.albums.add(tempalbum);
			    tilePane.getChildren().clear();
			    start(user);
			}else {
				ControllerTools.showInputError(5);
				return;
			}
		}
	}
	/**
	 * @author Joshua Pineda
	 * @param name - input album name
	 * @return - True if the album name has been used elsewhere, false if not.
	 * <p>Checks if an album name has been used yet</p>
	 */
	public boolean checkAlbumName(String name) {
		for(int i=0; i<user.getAlbums().size(); i++) {
			if(user.getAlbum(i).getName().equalsIgnoreCase(name)) {
				return true;
			}
		}
		return false;
	}
	/**
	 * @author Joshua Pineda John Strauser
	 * <p>Raises a dialog box prompting user to select (via drop down menu) which album they would like to delete</p>
	 */
	private void DeleteAlbumDialogBox() {
		List<String> choices = new ArrayList<>();
		for (int i = 0; i < user.getAlbums().size();i++) {
			
			choices.add(user.getAlbum(i).name);
		}

		ChoiceDialog<String> dialog = new ChoiceDialog<>("Select an album name", choices);
		dialog.setTitle("Delete an Album");
		dialog.setHeaderText("Album Deletion");
		dialog.setContentText("Choose an album to delete:");

		// Traditional way to get the response value.
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()){
			if (result.get().equals("Select an album name")) {
				ControllerTools.showInputError(2);
			}else {
				for (int i = 0; i < user.getAlbums().size(); i++) {
					if (user.getAlbum(i).name.equals(result.get())) {
						user.albums.remove(i);
					}
				}
			}
		}
	    tilePane.getChildren().clear();
	    start(user);
	}
	/**
	 * @author Joshua Pineda John Strauser
	 * @param u - Current User
	 * <p>Initializes the view of User's albums</p>
	 */
	public void start(User u) {
		//Get user
		user=u;
		
		//Tilepane setup
		tilePane.setPadding(new Insets(15,15,15,15));
		tilePane.setHgap(15);
		
		//Set images
		int size = user.getAlbums().size();
		for(int i=0; i<size; i++){
			//get first image in album
			Photo p = user.getAlbum(i).getPhoto(0);
			//create imageView from image
			ImageView iv = createImageView(p.getPath());
			
			//Create Label
			Label l = new Label(user.getAlbum(i).getName(),iv);
			l.setOnMouseClicked(new EventHandler<MouseEvent>(){

				@Override
				public void handle(MouseEvent arg0) {
					Label temp = (Label)arg0.getSource();
					int index = tilePane.getChildren().indexOf(temp);
					arg0.consume();
					albumViewHandler(index);
				}
				
			});
			
			l.setMaxSize(75, 70);
			l.setContentDisplay(ContentDisplay.TOP);
			//add to obslist within tilePane
			tilePane.getChildren().add(l);
		}
		//scrollPane setup
		scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
		scrollPane.setFitToWidth(true);
		scrollPane.setContent(tilePane);
	}
	/**
	 * @author John Strauser
	 * @param path - path to the image in user's space
	 * @return - Imageview
	 * <p>Creates an Imageview from a given path</p>
	 */
	public ImageView createImageView(String path){
		Image image = new Image(path);
		ImageView iv = new ImageView(image);
		
		iv.setFitWidth(50);
		iv.setFitHeight(50);
		
		return iv;
	}
	/**
	 * @author Joshua Pineda John Strauser
	 * <p>Change scene to Search page</p>
	 */
	public void searchHandler() {
		Photos.changeScene("/view/Search.fxml",user);
	}
	/**
	 * @author Joshua Pineda John Strauser
	 * <p>Log user out, change scene to the login page.</p>
	 */
	public void logoutHandler() {
		Photos.changeScene("/view/Login.fxml",0);
	}
	/**
	 * @author Joshua Pineda John Strauser
	 * <p>Raises a dialog box to handle creation of a new album</p>
	 */
	public void createAlbumHandler() {
		CreateAlbumDialogBox();
	}
	/**
	 * @author Joshua Pineda John Strauser
	 * <p>Raises a dialog box to handle deletion of user's album</p>
	 */
	public void deleteAlbumHandler() {
		DeleteAlbumDialogBox();
		
	}
	/**
	 * @author Joshua Pineda John Strauser
	 * <p>Changes scene to album view (of the selected image)</p>
	 * @param index - used to determine which album to view
	 */
	public void albumViewHandler(int index) {
		Photos.changeScene("/view/AlbumView.fxml", user.getAlbum(index));
	}
}
