package controller;
import model.Photos;
import model.User;
import javafx.fxml.FXML;

import java.util.List;

import javafx.collections.FXCollections;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 * 
 * @author Joshua Pineda John Strauser
 * <p> Controls functionality of the login screen</p>
 *
 */
public class LoginController {
	@FXML TextField Username;
	@FXML Button login;
	
	public List<User> users;
	
	/**
	 * <p>sets list of users (list of users saved internally)</p>
	 * @param u - list of users
	 */
	public void start(List<User> u) {
		users=u;
	}
	/**
	 * @author Joshua Pineda John Strauser
	 * <p>Validates username, either brings user to admin page (if admin), User's homepage, or an error dialog box is raised</p>
	 */
	public void loginHandler() {
		String user = Username.getText();
		//admin login, lead to admin page
		if (user.equals("admin")) {
			Photos.changeScene("/view/AdminPage.fxml", user);
		}else {
			//Handle null case 
			if (user.equals(null) || user.equals("")) {
				ControllerTools.showUserError(2,"");
				return;
			}
			//regular user
			else {
				//If user is in list of users, allow
				int index = containsUser(user);
				if(index != -1) {
					Photos.changeScene("/view/HomePage.fxml",users.get(index));
				}else {
					ControllerTools.showUserError(1, user);
					return;
				}
			}
		}
	}
	/**
	 * 
	 * @param user - username of the user
	 * @return - True if user is in the list of users (saved internally), false if not
	 */
	public int containsUser(String user) {
		for(int i = 0; i<users.size(); i++) {
			User u = users.get(i);
			if(u.getUsername().equals(user)) {
				return i;
			}
		}
		return -1;
	}
}
